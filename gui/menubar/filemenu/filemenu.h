#ifndef FILEMENU_H
#define FILEMENU_H

#include <QMenu>

class FileMenu : public QMenu {
 Q_OBJECT

 public:
  FileMenu(QWidget *parent = nullptr);
};

#endif // FILEMENU_H

#ifndef SETTINGSMENU_H
#define SETTINGSMENU_H

#include <siestacfgwidget/siestacfgwidget.h>

#include <QMenu>
#include <QString>

class SettingsMenu : public QMenu {
 Q_OBJECT

 public:
  SettingsMenu(QWidget *parent = nullptr);
  ~SettingsMenu();

 private:
  const char *kSiestaSectionName{ "Configure Siesta" };

  SiestaCfgWidget *siesta_cfg_widget_{ nullptr };

 private slots:
  void SiestaSectionActivated();
};

#endif // SETTINGSMENU_H

#include <config.h>

#include <menubar/settingsmenu/settingsmenu.h>

#include <QDialog>
#include <QFile>
#include <QTextStream>

SettingsMenu::SettingsMenu(QWidget *parent) 
  : QMenu(QString("Settings"), parent)
  , siesta_cfg_widget_(new SiestaCfgWidget()) {
  // add siesta configuration section to the settings menu
  this->addAction(QString(kSiestaSectionName), this, kSiestaSectionName);
  QObject::connect(this->actions().back(), &QAction::triggered,
                   this, &SettingsMenu::SiestaSectionActivated);

  // set up siesta cfg widget for further interactions
  siesta_cfg_widget_->setWindowTitle(QString(kSiestaSectionName));
  siesta_cfg_widget_->hide();
  return;
}

SettingsMenu::~SettingsMenu() {
  // release memory
  for (auto& action_it : this->actions()) {
    delete action_it;
  }
  delete siesta_cfg_widget_;
  return;
}


void SettingsMenu::SiestaSectionActivated() {
  siesta_cfg_widget_->show();
  return;
}

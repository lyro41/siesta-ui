#include <mainwindow/mainwindow.h>

#include <config.h>

#include <menubar/filemenu/filemenu.h>
#include <menubar/settingsmenu/settingsmenu.h>
#include <siestacfgwidget/siestacfgwidget.h>

#include <cstdlib>
#include <string>
#include <chrono>
#include <ctime>
#include <sstream>
#include <iomanip>

#include <QFileDialog>
#include <QString>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) 
  : QMainWindow(parent)
  , menu_bar_(new QMenuBar(this))
  , pbutton_calculate_(new QPushButton(kPButtonCalculateName, this)) {
    // check if command processor exists
    if (system(nullptr) == 0) {
      throw;    // TODO: show an exception
    } 
    // set up menu bar
    menu_bar_->addMenu(new FileMenu(this));
    menu_bar_->addMenu(new SettingsMenu(this));
    this->setMenuBar(menu_bar_);

    // set up calculate button
    pbutton_calculate_->move(100, 100);    // testing... / edit later

    // connect signals to slots
    QObject::connect(pbutton_calculate_, &QPushButton::clicked,
                     this, &MainWindow::Calculate);
    
    return;
}

MainWindow::~MainWindow() {
  // delete all the menus of the menu bar and then delete menu bar
  for (auto& action_it : menu_bar_->actions()) {
    delete action_it;
  }
  delete menu_bar_;

  return;
}


void MainWindow::Calculate() {
  QString siesta_path = SiestaCfgWidget::GetSiestaPath();
  if (siesta_path.isNull()) {
    throw;    // TODO: show an exception
  } else {
    CreateShellScript(siesta_path);
    // TODO: show waiting widget
    #ifdef __unix__
      system((std::string("sh ") + 
              EXECUTION_DIR + "/" + SHELL_SCRIPT_NAME).c_str());
    #endif
  }
  return;
}

void MainWindow::CreateShellScript(const QString &siesta_path) {
  QFile script_file(QString(EXECUTION_DIR) + "/" + SHELL_SCRIPT_NAME);
  if (!script_file.open(QIODevice::WriteOnly | 
                        QIODevice::Text |
                        QIODevice::Truncate)) {
    throw; // TODO: show an exception
  } else {
    QTextStream script_file_out(&script_file);
    // set the path to log file
    auto now = std::chrono::system_clock::now();
    auto time = std::chrono::system_clock::to_time_t(now);
    std::stringstream tmp("");
    // put date and time of execution at yyyy-mm-dd-hh:mm:ss format
    tmp << std::put_time(std::localtime(&time), "%F-%X");
    system((std::string("echo ") + tmp.str()).c_str());
    QString log_file_path(QString(LOG_DIR) + tmp.str().c_str() + ".log");
    #ifdef __unix__ 
    {
      // move to execution directory
      QString cmd_cd_execution("cd ");
      cmd_cd_execution += EXECUTION_DIR;
      script_file_out << cmd_cd_execution << '\n';

      // remove non-actual link to siesta executable
      QString cmd_rm_siesta_link("find ");
      cmd_rm_siesta_link += ".";
      cmd_rm_siesta_link += " -maxdepth 1";
      cmd_rm_siesta_link += " -type l";
      cmd_rm_siesta_link += " -executable";
      cmd_rm_siesta_link += " -name ";
      cmd_rm_siesta_link += SIESTA_EXE_NAME;
      cmd_rm_siesta_link += " -delete";
      script_file_out << cmd_rm_siesta_link << '\n';

      // make new link to siesta executable
      QString cmd_link_siesta("ln -s ");
      cmd_link_siesta += siesta_path + " ";
      script_file_out << cmd_link_siesta << '\n';

      // TODO: create calculation source files

      // start calculation process and restream error messages to log file
      QString cmd_siesta(QString("./") + SIESTA_EXE_NAME);
      (cmd_siesta += " < ") += SIESTA_SOURCE;
      (cmd_siesta += " > ") += SIESTA_OUTPUT;
      (cmd_siesta += " 2>") += log_file_path;
      script_file_out << cmd_siesta << '\n';

      // after calculation start parsing output files

      // move to SCRIPTS_DIR
      QString cmd_cd_scripts_dir(QString("cd ") + SCRIPTS_DIR);
      script_file_out << cmd_cd_scripts_dir << '\n';

      // check if SCRIPTS_DIR/data directory exists 
      // and create it if directory does not exist
      script_file_out << QString("ls data 1>/dev/null 2>/dev/null") << '\n' <<
                         QString("if [ $? -ne 0 ]") << '\n' <<
                         QString("then") << '\n' <<
                         QString("  mkdir data") << '\n' <<
                         QString("fi") << '\n';

      // copy output files to SCRIPTS_DIR
      QString cmd_copy_out_files(QString("cp ") + 
                                         EXECUTION_DIR + "/" + SIESTA_OUTPUT + 
                                         " data/" + SIESTA_OUTPUT);
      script_file_out << cmd_copy_out_files << '\n';

      // run scripts and restream error messages to log file
      QString cmd_parser_script(QString("python3 ") + PARSER_SCRIPT +
                                " 2>" + log_file_path);
      script_file_out << cmd_parser_script << '\n';
    } 
    #endif
  }
}

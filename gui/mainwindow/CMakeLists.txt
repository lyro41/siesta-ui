add_library(mainwindow mainwindow.h mainwindow.cpp)

target_link_libraries(mainwindow Qt5::Widgets)
target_link_libraries(mainwindow filemenu)
target_link_libraries(mainwindow settingsmenu)

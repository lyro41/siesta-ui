#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QMenuBar>

class MainWindow : public QMainWindow { 
 Q_OBJECT

 signals:
  void SendCalculationResult(int result_code);

 public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 private:
  const char *kPButtonCalculateName{ "Calculate" };

  QMenuBar *menu_bar_{ nullptr };
  QPushButton *pbutton_calculate_{ nullptr };

 private slots:
  static void Calculate();
  static void CreateShellScript(const QString &siesta_path);
};

#endif // MAINWINDOW_H

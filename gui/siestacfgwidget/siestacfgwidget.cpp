#include <config.h>

#include <siestacfgwidget/siestacfgwidget.h>

#include <QFile>
#include <QTextStream>

SiestaCfgWidget::SiestaCfgWidget(QWidget *parent)
  : QWidget(parent)
  , layout_(new QVBoxLayout(this))
  , ledit_siesta_path_(new QLineEdit(this))
  , pbutton_browse_(new QPushButton(kPButtonBrowseName, this))
  , fdialog_(new QFileDialog(this))
  , errmsg_siesta_path_invalid_(new QErrorMessage(this)) {
  // set siesta.cfg file needed permissions
  QFile siesta_cfg(SIESTA_CONFIG);
  if (!siesta_cfg.open(QIODevice::ReadOnly | QIODevice::Text)) {
    siesta_cfg.open(QIODevice::WriteOnly | QIODevice::Text);
  }
  siesta_cfg.close();
  if (!siesta_cfg.setPermissions(QFileDevice::ReadOwner  |
                                 QFileDevice::WriteOwner |
                                 QFileDevice::ReadUser   |
                                 QFileDevice::WriteUser  |
                                 QFileDevice::ReadGroup  |
                                 QFileDevice::WriteGroup |
                                 QFileDevice::ReadOther)) {
    throw;
  }

  // configure siesta path line edit
  ledit_siesta_path_->setText(GetSiestaPath());

  // configure browse push button

  // configure file dialog
  fdialog_->setFilter(QDir::AllEntries);

  // configure siesta path validity error message dialog

  // connect signals to slots
  QObject::connect(pbutton_browse_, &QPushButton::clicked,
                   this, &SiestaCfgWidget::PButtonBrowseClicked);
  QObject::connect(fdialog_, &QFileDialog::fileSelected,
                   this, &SiestaCfgWidget::FDialogFileSelected);

  // set up layout
  layout_->addWidget(ledit_siesta_path_);
  layout_->addWidget(pbutton_browse_);
  layout_->addWidget(fdialog_);
  this->setLayout(layout_);

  return;
}

SiestaCfgWidget::~SiestaCfgWidget() {
  // transfer configuration properties to the siesta.cfg file
  if (CheckSiestaPathValidity(ledit_siesta_path_->text())) {
    QFile siesta_cfg(SIESTA_CONFIG);
    siesta_cfg.open(QIODevice::WriteOnly | 
                    QIODevice::Text);
    
    QTextStream siesta_cfg_fout(&siesta_cfg);
    siesta_cfg_fout << SIESTA_CFG_KEY_PATH << ' ' << 
                       ledit_siesta_path_->text() << '\n';
    siesta_cfg_fout.flush();
    siesta_cfg.close();
  }

  // release memory
  delete ledit_siesta_path_;
  delete layout_;
  delete pbutton_browse_;
  delete fdialog_;
  delete errmsg_siesta_path_invalid_;
  return;
}


QString SiestaCfgWidget::GetSiestaPath() {
  QFile siesta_cfg(SIESTA_CONFIG);
  // check if siesta.cfg contains information about executable path
  QString siesta_path = QString();
  if (siesta_cfg.open(QIODevice::ReadOnly | QIODevice::Text)) {
    QTextStream siesta_cfg_fin(&siesta_cfg);
    while (!siesta_cfg_fin.atEnd()) {
      QString key;
      siesta_cfg_fin >> key;
      if (key == SIESTA_CFG_KEY_PATH) {
        QString buff = siesta_cfg_fin.readLine();
        buff.remove(' ');
        if (CheckSiestaPathValidity(buff)) {
          siesta_path = buff;
        }
      }
    }
  }
  siesta_cfg.close();
  return siesta_path;
}


void SiestaCfgWidget::closeEvent(QCloseEvent *event) {
  if (CheckSiestaPathValidity(ledit_siesta_path_->text())) {
    event->accept();
  } else {
    if (!errmsg_siesta_path_invalid_->isVisible()) {
      errmsg_siesta_path_invalid_->showMessage(ERR_MSG_SIESTA_PATH);
    }
    event->ignore();
  }
  return;
}


bool SiestaCfgWidget::CheckSiestaPathValidity(const QString &path) {
  QFile file_test(path);
  if (path.endsWith(SIESTA_EXE_NAME) && 
      file_test.open(QIODevice::ReadOnly | QIODevice::Text)) {
    file_test.close();
    return true;
  }
  return false;
}


void SiestaCfgWidget::PButtonBrowseClicked() {
  fdialog_->exec();
  return;
}

void SiestaCfgWidget::FDialogFileSelected(const QString &path) {
  if (CheckSiestaPathValidity(path)) {
    ledit_siesta_path_->setText(path);
  }
  return;
}

#ifndef SIESTACFGWIDGET_H // abbreviation of Siesta Configuration Widget
#define SIESTACFGWIDGET_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFileDialog>
#include <QCloseEvent>
#include <QErrorMessage>
#include <QString>

class SiestaCfgWidget : public QWidget {
 Q_OBJECT

 public:
  SiestaCfgWidget(QWidget *parent = nullptr);
  ~SiestaCfgWidget();

  static QString GetSiestaPath();

 protected:
  virtual void closeEvent(QCloseEvent *event) override;

 private:
  const char *kPButtonBrowseName{ "..." };

  QVBoxLayout *layout_{ nullptr };
  QLineEdit *ledit_siesta_path_{ nullptr };
  QPushButton *pbutton_browse_{ nullptr };
  QFileDialog *fdialog_{ nullptr };
  QErrorMessage *errmsg_siesta_path_invalid_{ nullptr };

  static bool CheckSiestaPathValidity(const QString &path);

 private slots:
  void PButtonBrowseClicked();
  void FDialogFileSelected(const QString &path);
};

#endif // SIESTACFGWIDGET_H

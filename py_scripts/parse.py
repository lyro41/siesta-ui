import os
import string
import matplotlib
import matplotlib.pyplot as plot
import glob
import numpy
import math


def concatenate(words):
  result = ''
  for word in words:
    result += word + ' '
  result = result[:-1]
  return result


def bool_def(sentence):
  if sentence == 'T':
    return True
  elif sentence == 'F':
    return False
  else:
    raise


def graph_CG_opt_move(name, tolerance, unit, xkey, ykey):
  global CG_data
  y_max = 0.0

  plot.title(name + " convergence to " + str(tolerance) + ' ' + unit)
  plot.xlabel(xkey)
  plot.ylabel(ykey)
  plot.grid()

  for i in range(len(CG_data)):
    xpoints = numpy.array(CG_data[i][xkey])
    ypoints = numpy.array(CG_data[i][ykey])
    y_max = max(y_max, ypoints[ypoints.argmax()])
    if xpoints.size == 1:
      plot.plot(xpoints, ypoints, marker = 'o')
    else:
      plot.plot(xpoints, ypoints)
    
  y_max = math.ceil(y_max)
  plot.ylim(bottom = tolerance, top = y_max)
  plot.savefig("graph/" + name + ".total.png")
  plot.clf()

  for i in range(len(CG_data)):  
    plot.title(name + " convergence to " + str(tolerance) + ' ' + unit)
    plot.xlabel(xkey)
    plot.ylabel(ykey)
    plot.grid()
    plot.ylim(bottom = tolerance, top = y_max)

    xpoints = numpy.array(CG_data[i][xkey])
    ypoints = numpy.array(CG_data[i][ykey])

    if xpoints.size == 1:
      plot.plot(xpoints, ypoints, marker = 'o')
    else:
      plot.plot(xpoints, ypoints)
    plot.savefig("graph/" + name + "." + str(i) + ".png")
    plot.clf()
    
  return



if len(glob.glob("graph")) == 0:
  os.mkdir("graph")

out_fdf = open(glob.glob("data/*.out")[0], "r")

cfg_dict = {}
lines = out_fdf.read().split('\n')

# parse data 

size = len(lines)
i = 0
for i in range(size):
  line_wo_comms = lines[i].split('#')[0]
  words = line_wo_comms.split()
  if len(words) == 0: 
    continue

  if words[0] == "Begin" and concatenate(words[:-2]) == "Begin CG opt. move":
    i += 1
    break
  
  if words[0] == "SystemName":
    cfg_dict[words[0]] = concatenate(words[1:]) 
  elif words[0] == "SystemLabel":
    cfg_dict[words[0]] = concatenate(words[1:])
  elif words[0] == "redata:":
    statement = concatenate(words[1:]).split(' = ')
    if len(statement) == 2:
      cfg_dict[statement[0]] = statement[1]
  

# parse CG optimization

it_num = 0
flag = False
params = []
CG_data = []
Fmax = []
for i in range(i, size):
  line_wo_comms = lines[i].split('#')[0]
  words = line_wo_comms.split()

  if len(words) == 0:
    continue
  
  if words[0] == "outcoor:":
    break

  if words[0] == "Begin":
    it_num += 1
  elif words[0] == "iscf":
    params = words
    flag = True
    CG_data.append({})
    for word in words:
      CG_data[it_num][word] = []
  elif words[0] == "SCF":
    flag = False
  elif words[0] == "scf:":
    CG_data[it_num][params[0]].append(int(words[1]))
    for j in range(1, len(params)):
      CG_data[it_num][params[j]].append(float(words[j + 1]))
  elif words[0] == "Max" and words[-1] == "constrained":
    Fmax.append(float(words[1]))


# graph CG optimization steps

Harris_flag = bool_def(cfg_dict["Require Harris convergence for SCF"])
H_flag = bool_def(cfg_dict["Require H convergence for SCF"])
DM_flag = bool_def(cfg_dict["Require DM convergence for SCF"])

if Harris_flag:
  Harris_tolerance, Harris_unit = cfg_dict["Harris energy tolerance for SCF"].split()
  Harris_tolerance = float(Harris_tolerance)
  graph_CG_opt_move("Harris", Harris_tolerance, Harris_unit, params[0], params[1])
      
if H_flag:
  H_tolerance, H_unit = cfg_dict["Hamiltonian tolerance for SCF"].split()
  H_tolerance = float(H_tolerance)
  graph_CG_opt_move("Hamiltonian", H_tolerance, H_unit, params[0], params[6])

if DM_flag:
  DM_tolerance = cfg_dict["DM tolerance for SCF"]
  DM_tolerance = float(DM_tolerance)
  graph_CG_opt_move("DM", DM_tolerance, "", params[0], params[4])


# graph Fmax convergence cycle

F_tolerance, F_unit = cfg_dict["Force tolerance"].split()
F_tolerance = float(F_tolerance)

plot.title("Fmax convergence to " + cfg_dict["Force tolerance"])
plot.xlabel("CG opt. move")
plot.ylabel("Fmax(" + F_unit + ")")
plot.ylim(bottom = F_tolerance, top = math.ceil(max(Fmax)))
plot.grid()

if len(Fmax) == 1:
  plot.plot(numpy.array(Fmax), marker = 'o')
else:
  plot.plot(numpy.array(Fmax))
plot.savefig("graph/Fmax.png")
plot.clf()


#print(CG_data)

#print(cfg_dict)
